[[_TOC_]]

## How to

### export

Avoid default export, see [Axel's post](https://medium.com/@rauschma/note-that-default-exporting-objects-is-usually-an-anti-pattern-if-you-want-to-export-the-cf674423ac38)